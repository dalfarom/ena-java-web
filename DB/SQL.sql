

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ENA
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ENA
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ENA` DEFAULT CHARACTER SET utf8 ;
USE `ENA` ;

-- -----------------------------------------------------
-- Table `ENA`.`gerencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ENA`.`gerencia` (
  `idgerencia` INT NOT NULL AUTO_INCREMENT,
  `gerencia` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idgerencia`),
  UNIQUE INDEX `idgerencia_UNIQUE` (`idgerencia` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ENA`.`departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ENA`.`departamento` (
  `iddepartamento` INT NOT NULL AUTO_INCREMENT,
  `departamento` VARCHAR(45) NOT NULL,
  `gerencia` INT NOT NULL,
  PRIMARY KEY (`iddepartamento`),
  UNIQUE INDEX `iddepartamento_UNIQUE` (`iddepartamento` ASC) ,
  INDEX `fk_departamento_gerencia_idx` (`gerencia` ASC) ,
  CONSTRAINT `fk_departamento_gerencia`
    FOREIGN KEY (`gerencia`)
    REFERENCES `ENA`.`gerencia` (`idgerencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ENA`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ENA`.`cargo` (
  `idcargo` INT NOT NULL AUTO_INCREMENT,
  `cargo` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idcargo`),
  UNIQUE INDEX `idcargo_UNIQUE` (`idcargo` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ENA`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ENA`.`persona` (
  `idpersona` INT NOT NULL AUTO_INCREMENT,
  `primerNombre` VARCHAR(50) NOT NULL,
  `segundoNombre` VARCHAR(50) NOT NULL,
  `apellidoPaterno` VARCHAR(50) NOT NULL,
  `apellidoMaterno` VARCHAR(50) NOT NULL,
  `departamento` INT NOT NULL,
  `cargo` INT NOT NULL,
  PRIMARY KEY (`idpersona`),
  UNIQUE INDEX `idpersona_UNIQUE` (`idpersona` ASC) ,
  INDEX `fk_persona_departamento1_idx` (`departamento` ASC) ,
  INDEX `fk_persona_cargo1_idx` (`cargo` ASC) ,
  CONSTRAINT `fk_persona_departamento1`
    FOREIGN KEY (`departamento`)
    REFERENCES `ENA`.`departamento` (`iddepartamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_cargo1`
    FOREIGN KEY (`cargo`)
    REFERENCES `ENA`.`cargo` (`idcargo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ENA`.`requerimiento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ENA`.`requerimiento` (
  `idrequerimiento` INT NOT NULL AUTO_INCREMENT,
  `requerimiento` MEDIUMTEXT NOT NULL,
  `estado` TINYINT NOT NULL,
  `encargado` INT NOT NULL,
  PRIMARY KEY (`idrequerimiento`),
  UNIQUE INDEX `idrequerimiento_UNIQUE` (`idrequerimiento` ASC) ,
  INDEX `fk_requerimiento_persona1_idx` (`encargado` ASC) ,
  CONSTRAINT `fk_requerimiento_persona1`
    FOREIGN KEY (`encargado`)
    REFERENCES `ENA`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
