<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Menu</title>
	<!-- Carga de CSS (Bulma CSS) -->
	<link rel="stylesheet" type="text/css" href="./assets/css/bulma.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/personal.css">
	<link rel="stylesheet" href="./assets/css/font-awesome.css">
</head>
<body>
	<div class="box login">
		<h1 class="title">Menu</h1>
		<hr>
		<a class="button is-medium is-fullwidth elementoMenu" href="ingresar-req.jsp">Ingresar Requerimiento</a>
		<a class="button is-medium is-fullwidth elementoMenu" href="consultar-req.jsp">Consultar Requerimiento</a>
		<a class="button is-medium is-fullwidth elementoMenu" href="cerrar-req.jsp">Cerrar Requerimiento</a>	
	</div>
</body>
</html>