<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Error de Credenciales</title>
	<!-- Carga de CSS (Bulma CSS) -->
	<link rel="stylesheet" type="text/css" href="./assets/css/bulma.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/personal.css">
</head>
<body>

	<div class="box login">
		<h2 class="has-text-centered"><strong>ERROR!</strong></h2>
  		<p class="has-text-justified">Las credenciales ingresadas son erroneas porfavor verifique y reintente</p>
  		<center><a class="button is-warning" href="index.jsp">Volver</a></center>	
	</div>

</body>
</html>