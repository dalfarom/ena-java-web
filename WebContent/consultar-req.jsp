<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="db.Mysql"%>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Consultar Requerimiento</title>
	<!-- Carga de CSS (Bulma CSS) -->
	<link rel="stylesheet" type="text/css" href="./assets/css/bulma.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/personal.css">
	<script type="text/javascript" src="./assets/js/jquery-3.3.1.js"></script>
</head>
<body>
	
	<div class="box login">
		<h1 class="title">Ingresar Requerimiento</h1>
		<hr>
		<form action="GuardarReq" method="post">
		
			<div class="field is-horizontal">
  				<div class="field-label is-normal">
    				<label class="label">Gerencia</label>
  				</div>
  				<div class="field-body">
    				<div class="field">
      					<p class="control">
        					<div class="select" style="width: 100%;">
  								<select style="width: 100%;" id="gerenciaInput" name="gerencia" required>
  									<option>Seleccione una Opcion</option>
  									<%
	
										Mysql conexion = new Mysql();		

										try {
											Statement st = conexion.conexion().createStatement();
	
											ResultSet gerencias = st.executeQuery("select * from gerencia");
		
											while(gerencias.next()){
											
												out.println("<option value="+gerencias.getObject("idgerencia")+">"+gerencias.getObject("gerencia")+"</option>");
											}
		
											} catch (SQLException e) {
	
												e.printStackTrace();
											}

											try {
												conexion.conexion().close();
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

									%>
  								</select>
							</div>
	      				</p>
    				</div>
  				</div>
			</div>
			<div class="field is-horizontal">
  				<div class="field-label is-normal">
    				<label class="label">Departamento</label>
  				</div>
  				<div class="field-body">
    				<div class="field">
      					<p class="control">
        					<div class="select" style="width: 100%;">
  								<select style="width: 100%;" name="depto" required>
    								<option>Seleccione una opcion</option>
    								<%
	
										Mysql conexion1 = new Mysql();		

										try {
											Statement st = conexion1.conexion().createStatement();
	
											ResultSet gerencias = st.executeQuery("select * from departamento");
		
											while(gerencias.next()){
											
												out.println("<option value="+gerencias.getObject("iddepartamento")+">"+gerencias.getObject("departamento")+"</option>");
											}
		
											} catch (SQLException e) {
	
												e.printStackTrace();
											}

											try {
												conexion.conexion().close();
											} catch (SQLException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

									%>

  								</select>
							</div>
      					</p>
    				</div>
  				</div>
			</div>
			<button class="button" style="margin-top: 11px;" type="submit">Buscar</button>
			
		
		</form>
		
		<table class="table is-bordered is-fullwidth" style="margin-top: 11px;">
			<tr>
    			<th>ID</th>
    			<th>Descripcion</th> 
    			<th>Estado</th>
  			</tr>
  			<%
	
				Mysql conexion3 = new Mysql();		

				try {
				Statement st = conexion1.conexion().createStatement();
	
				ResultSet requerimiento = st.executeQuery("select * from requerimiento");
		
				while(requerimiento.next()){
											
					out.println("<tr>");
					out.print("<td>"+requerimiento.getObject("idrequerimiento")+"</td>");
					out.print("<td>"+requerimiento.getObject("requerimiento")+"</td>");
					if(requerimiento.getInt("estado") == 0){
						out.print("<td>Abierto</td>");
					}else{
						out.print("<td>Cerrado</td>");
					}
					
					out.print("</tr>");
				}
		
				} catch (SQLException e) {
	
					e.printStackTrace();
				}

				try {
					conexion.conexion().close();
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}

			%>
		</table>
		<a class="button" style="margin-top: 11px;" href="menu.jsp">Volver al menu</a>
	</div>

<script>

/*

	$("#gerenciaInput").change(function(){
	var gerencia = {"gerencia": $("#gerenciaInput").val()};
	console.log(gerencia);

	    

	    $.ajaxSetup(
	        {
	            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	        });


	        // Cuerpo de Ajax
	        var url = 'http://localhost:8080/ENA/AccIngresar';
	    $.ajax(
	    {
	        url: url,
	        cache: false,
	        type: "GET",
	        data: gerencia,
	        dataType: "JSON",
	        success: function(data){

	        },
	        error: function(jqXHR, textStatus, errorThrown){}

	    });

	
	
})
*/
</script>
</body>
</html>