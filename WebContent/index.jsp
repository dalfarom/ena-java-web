<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Login</title>
	<!-- Carga de CSS (Bulma CSS) -->
	<link rel="stylesheet" type="text/css" href="./assets/css/bulma.css">
	<link rel="stylesheet" type="text/css" href="./assets/css/personal.css">
</head>
<body>

		<div class="columns ">
			<div class="column ">
				<div class="box login">
				<h1 class="title">Autentificación</h1>
				<hr>
  					<form action="Login" method="post">
  					<div class="field is-horizontal">
  						<div class="field-label is-normal">
    						<label class="label">Usuario</label>
  						</div>
  						<div class="field-body">
    						<div class="field">
      							<p class="control">
        							<input class="input" type="text" name="usuario" required placeholder="Usuario">
      							</p>
    						</div>
  						</div>
					</div>
					<div class="field is-horizontal">
  						<div class="field-label is-normal">
    						<label class="label">Password</label>
  						</div>
  						<div class="field-body">
    						<div class="field">
      							<p class="control">
        							<input class="input" type="password" name="password" required placeholder="Password">
      							</p>
    						</div>
  						</div>
					</div>
					<center><button class="button is-info is-rounded" type="submit">Ingresar</button></center>
					</form>
				</div>
			</div>
		</div>
	
</body>
</html>