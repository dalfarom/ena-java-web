package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Mysql {
	

	private String nombreBaseDatos = "ena";
	private String url = "jdbc:mysql://localhost:3306/"+nombreBaseDatos+"?autoReconnect=true&useSSL=false";
	private Connection con;
	//private Statement st;
	//private ResultSet rs;
	private String usuario ="root";
	private String contrasena ="";
	
	public Connection conexion() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection(url,usuario, contrasena);
			
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return con;
	}
	
	
		
}