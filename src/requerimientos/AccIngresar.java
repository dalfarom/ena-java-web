package requerimientos;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.Mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;


/**
 * Servlet implementation class AccIngresar
 */
@WebServlet("/AccIngresar")
public class AccIngresar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccIngresar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		
		int gerencia = Integer.parseInt(request.getParameter("gerencia"));

		
		Mysql conexion = new Mysql();		
		
		try {
			Statement st = conexion.conexion().createStatement();
			
			ResultSet validacion = st.executeQuery("(select * from departamento where gerencia = "+gerencia+")");
			
			String result = validacion.toString();
			
			
			
			out.print(result);
			out.flush();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		try {
			conexion.conexion().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
