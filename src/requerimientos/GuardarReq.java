package requerimientos;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.Mysql;

/**
 * Servlet implementation class GuardarReq
 */
@WebServlet("/GuardarReq")
public class GuardarReq extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuardarReq() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		// Lineas de Codigo Obligatorias
				response.setContentType("text/html"); 
				PrintWriter out = response.getWriter();
				
				String requerimiento = request.getParameter("requerimiento");
				int encargado = Integer.parseInt(request.getParameter("encargado"));
				
				Mysql conexion = new Mysql();		
				
				try {
					Statement st = conexion.conexion().createStatement();
					
					st.executeUpdate("insert into requerimiento (idrequerimiento, requerimiento, estado, encargado) values (null, '"+requerimiento+"', 0, "+encargado+")");
					
					
					response.sendRedirect("menu.jsp");
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
				
				try {
					conexion.conexion().close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	}

}
